#include "CppUTest/TestHarness.h"
extern "C" {
#include "LedDriver.h"
}

TEST_GROUP(LedDriver) {
  uint16_t virtualLeds;
  void setup() {
    LedDriver_Create(&virtualLeds, false);
  }
  void tearDown() {
  }
};

TEST(LedDriver, LedsOffAfterCreate) {
  uint16_t virtualLeds = 0xffff;
  LedDriver_Create(&virtualLeds, false);
  LONGS_EQUAL(0, virtualLeds);
}

TEST(LedDriver, TurnOnLedOne) {
  LedDriver_TurnAllOff();
  LedDriver_TurnOn(1);
  CHECK(LedDriver_IsOn(1) == true);
}

TEST(LedDriver, TurnOffLedOne) {
  LedDriver_TurnAllOff();
  LedDriver_TurnOn(1);
  LedDriver_TurnOff(1);
  CHECK(LedDriver_IsOff(1) == true);
}

TEST(LedDriver, TurnOnMultipleLeds) {
  LedDriver_TurnAllOff();
  LedDriver_TurnOn(9);
  LedDriver_TurnOn(8);
  CHECK(LedDriver_IsOn(9) == true);
  CHECK(LedDriver_IsOn(8) == true);
}

TEST(LedDriver, AllOn) {
  LedDriver_TurnAllOn();
  LONGS_EQUAL(0xffff, virtualLeds);
}

TEST(LedDriver, AllOff) {
  LedDriver_TurnAllOn();
  LedDriver_TurnAllOff();
  LONGS_EQUAL(0, virtualLeds);
}

TEST(LedDriver, TurnOffAnyLed) {
  LedDriver_TurnAllOn();
  LedDriver_TurnOff(8);
  CHECK(LedDriver_IsOff(8) == true);
}

TEST(LedDriver, LedMemoryIsNotReadable) {
  virtualLeds = 0xffff;
  LedDriver_TurnOn(8);
  LONGS_EQUAL(0x80, virtualLeds);
}

TEST(LedDriver, UpperAndLowerBounds) {
  LedDriver_TurnAllOff();
  LedDriver_TurnOn(1);
  LedDriver_TurnOn(16);
  LONGS_EQUAL(0x8001, virtualLeds);
}

TEST(LedDriver, OutOfBoundsTurnOnDoesNoHarm) {
  LedDriver_TurnOn(-1);
  LedDriver_TurnOn(0);
  LedDriver_TurnOn(17);
  LedDriver_TurnOn(3141);
  LONGS_EQUAL(0, virtualLeds);
}

TEST(LedDriver, OutOfBoundsTurnOffDoesNoHarm) {
  LedDriver_TurnAllOn();
  LedDriver_TurnOff(-1);
  LedDriver_TurnOff(0);
  LedDriver_TurnOff(17);
  LedDriver_TurnOff(3141);
  LONGS_EQUAL(0xffff, virtualLeds);
}

IGNORE_TEST(LedDriver, OutOfBoundsToDo) {
  // TODO: what to do during runtime?
}

IGNORE_TEST(LedDriver, OutOfBoundsProducesRuntimeError) {
  // TODO: what to do during runtime?
}

TEST(LedDriver, IsOn) {
  CHECK_FALSE(LedDriver_IsOn(11));
  LedDriver_TurnOn(11);
  CHECK(LedDriver_IsOn(11) == true);
}

TEST(LedDriver, OutOfBoundsLedsAreAlwaysOff) {
  CHECK_FALSE(LedDriver_IsOn(0));
  CHECK_FALSE(LedDriver_IsOn(17));
  CHECK(LedDriver_IsOff(0));
  CHECK(LedDriver_IsOff(17));
}

TEST(LedDriver, IsOff) {
  CHECK(LedDriver_IsOff(12) == true);
  LedDriver_TurnOn(12);
  CHECK_FALSE(LedDriver_IsOff(12));
}

TEST(LedDriver, TurnOffMultipleLeds) {
  LedDriver_TurnAllOn();
  LedDriver_TurnOff(9);
  LedDriver_TurnOff(8);
  CHECK(LedDriver_IsOff(8) == true);
  CHECK(LedDriver_IsOff(9) == true);
}


TEST_GROUP(LedDriverInverted) {
  uint16_t virtualLeds;
  void setup() {
    LedDriver_Create(&virtualLeds, true);
  }
  void tearDown() {
  }
};

TEST(LedDriverInverted, LedsOffAfterCreateInverted) {
  uint16_t virtualLeds = 0;
  LedDriver_Create(&virtualLeds, true);
  LONGS_EQUAL(0xffff, virtualLeds);
}

TEST(LedDriverInverted, AllOnInverted) {
  LedDriver_TurnAllOff();
  LedDriver_TurnAllOn();
  LONGS_EQUAL(0, virtualLeds);
}

TEST(LedDriverInverted, AllOffInverted) {
  LedDriver_TurnAllOn();
  LedDriver_TurnAllOff();
  LONGS_EQUAL(0xffff, virtualLeds);
}

TEST(LedDriverInverted, TurnLedTwoOnInverted) {
  LedDriver_TurnAllOff();
  LedDriver_TurnOn(2);
  LONGS_EQUAL((~0x0002) & 0xffff, virtualLeds)
}

TEST(LedDriverInverted, TurnLedTwoLedsOnInverted) {
  LedDriver_TurnAllOff();
  LedDriver_TurnOn(2);
  LedDriver_TurnOn(13);
  LONGS_EQUAL((~0x1002) & 0xffff, virtualLeds);
}

TEST(LedDriverInverted, TurnLedTwoLedsOffInverted) {
  LedDriver_TurnAllOn();
  LedDriver_TurnOff(2);
  LedDriver_TurnOff(13);
  LONGS_EQUAL(0x1002, virtualLeds);
}
