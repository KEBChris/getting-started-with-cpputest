# Getting Started with CppUtest

Documentation and a simple example project detailing how to build and start using CppUTest

## Installing

### My environment

* windows 7 x64
* cmake v3.13.2 ([download cmake](https://cmake.org/download/))
* MinGW Distro from [here](https://nuwen.net/mingw.html)
* all terminal commands an run using [git-bash](https://git-scm.com/)

### Clone CppUTest

**all terminal commands an run using [git-bash](https://git-scm.com/)**

```bash
> git clone https://github.com/CppUTest/CppUTest
```

### Building

I was unable to directly follow the install instructions provided in the [CppUTest repo](https://github.com/CppUTest/CppUTest) readme.

Running `autoge.sh` gave me the following error `autogen.sh: line 7: autoreconf: command not found`.

Using `cmake` as described in the readme lead to an error regarding not being able to find `cl.exe`

I instead did the following to successfully build CppUTest:

In the CppUTest directory,

```bash
> mkdir cpputest_build && cd cpputest_build
```

```bash
> cmake -G"Unix Makefiles" ..
```

```bash
> make -j4
```

The CppUTest library files will be located at:

`~/CppUTest/cpputest_build/src/CppUTest/libCppUTest.a`

`~/CppUTest/cpputest_build/src/CppUTestExt/libCppUTestExt.a`

## Using CppUTest

This repo contains the tests and source that was created while going through the first few
chapters of "Test-Drive Development for Embedded C" by James W. Greening.

To run the tests:

```bash
cd test/leddriver
make
```

`make` should build the tests and run them, the following output should be seen

```(bash)
g++ -c -o build/LedDriverTest.o LedDriverTest.cpp -I../../vendor/cpputest/include -I../../src

g++ -c -o build/AllTests.o AllTests.cpp -I../../vendor/cpputest/include -I../../src

g++ -c -o build/LedDriver.o ../../src/LedDriver.c -I../../vendor/cpputest/include -I../../src

g++ -o build/LedDriverTest build/LedDriverTest.o build/AllTests.o build/LedDriver.o -I../../vendor/cpputest/include -I../../src ../../vendor/cpputest/lib/libCppUTest.a -lstdc++

./build/LedDriverTest
..........!!...........
OK (23 tests, 21 ran, 28 checks, 2 ignored, 0 filtered out, 1 ms)
```

### test folder explanation

The tests are located at `test/leddriver/LedDriverTest.cpp`

When the tests are built their output will be at `test/leddriver/build`

The `AllTests.cpp` is the entry point for the executable that runs the tests -- `main()` is located in this file.

The `AllTests.h` contains the test groups to include.

### makefile

The makefile's default rule is to build and run the tests.

The makefile should be configured to include the `CppUTest` include folder as well as
link in the `libCppUTest.a` -- both of these are located in `vendor/cpputest`.

## Helpful links and Sources

[CppUTest website](https://CppUTest.github.io/index.html)

[CppUTest github](https://github.com/CppUTest/CppUTest)

[CppUTest slide tutorial](https://www.odd-e.com/material/2009/JAOO_TDD_C_Tutorial/TDD_in_C_JAOO_tutorial.pdf)

2/25/2019 Chris Owens
