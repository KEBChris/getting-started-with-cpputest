#include "LedDriver.h"
enum { 
  ALL_LEDS_ON = ~0,
  ALL_LEDS_OFF = ~ALL_LEDS_ON,
};

static uint16_t* ledsAddress;
static uint16_t ledsImage;
static bool inverted;

static uint16_t convertLedNumberToBit(int ledNumber);
static void updateHardware(void);
static bool isLedOutOfBounds(int ledNumber);
static void setLedImaageBit(int ledNumber);
static void clearLedImaageBit(int ledNumber);

void LedDriver_Create(uint16_t* address, bool isInverted) {
  ledsAddress = address;
  inverted = isInverted;
  LedDriver_TurnAllOff();
  updateHardware();
}

void LedDriver_Destroy(void) {
}

void LedDriver_TurnOn(int ledNumber) {
  if (isLedOutOfBounds(ledNumber) == true) {
    return;
  }
  setLedImaageBit(ledNumber);
  updateHardware();
}

void LedDriver_TurnOff(int ledNumber) {
  if (isLedOutOfBounds(ledNumber) == true) {
    return;
  }
  clearLedImaageBit(ledNumber);
  updateHardware();
}

void LedDriver_TurnAllOn(void) {
  ledsImage = ALL_LEDS_ON;
  updateHardware();
}

void LedDriver_TurnAllOff(void) {
  ledsImage = ALL_LEDS_OFF;
  updateHardware();
}
bool LedDriver_IsOn(int ledNumber) {
  if (isLedOutOfBounds(ledNumber) == true) {
    return false;
  }
  return ledsImage & (convertLedNumberToBit(ledNumber));
}

bool LedDriver_IsOff(int ledNumber) {
  return !LedDriver_IsOn(ledNumber);
}
static uint16_t convertLedNumberToBit(int ledNumber) {
  return (1 << (ledNumber - 1));
}

static void updateHardware(void) {
#if 1
  if (inverted == true) {
    *ledsAddress = ~ledsImage;
  }
  else {
    *ledsAddress = ledsImage;
  }
#else
  *ledsAddress = ledsImage;
#endif
}

static bool isLedOutOfBounds(int ledNumber) {
  bool retVal = false;
  if (ledNumber <= 0 || ledNumber > 16) {
    retVal = true;
  }
  return retVal;
}

static void setLedImaageBit(int ledNumber) {
  ledsImage |= convertLedNumberToBit(ledNumber);
}
static void clearLedImaageBit(int ledNumber) {
  ledsImage &= ~convertLedNumberToBit(ledNumber);
}
